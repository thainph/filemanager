# Filemanager

A file manager for laravel.

## Installation

1. Publish config: `php artisan vendor:publish --tag=file-manager-config`
2. Publish assets: `php artisan vendor:publish --tag=file-manager-assets`
3. Publish views: `php artisan vendor:publish --tag=file-manager-views`
4. Config guards, upload max size, extensions... in `config/file-manager.php`

## Access file manager

When you want to open file manager, access to `{your-domain}/file-manager/{guard}/{id}`

## Use for CKeditor
Config CKeditor:
```
const options = {
    filebrowserImageBrowseUrl: {your-domain}/file-manager/{guard}/{id}?type=ckeditor,
    filebrowserImageUploadUrl: {your-domain}/file-manager/{guard}/{id}/upload,
    window.CKEDITOR.replace(editor, options);
};
```

## Use for TinyMCE
Config TinyMCE:
```
tinymce.init({
    selector: 'textarea',
    plugins: 'image',
    toolbar: 'image',
    image_title: true,
    automatic_uploads: true,
    file_picker_types: 'image',
    file_picker_callback: function(callback, value, meta) {
        window.fileManagerClient = {
            onSelected(payload) {
                callback(payload.url);
            }
        };
        window.open('{your-domain}/file-manager/{guard}/{id}?type=tinymce', 'FileManager', 'width=900,height=600');
    }
});
``` 
## User for selector

Embed your js like:

```
window.fileManagerClient = {
    onSelected(payload) {
        // Write your code here
    }
};
```
When you want to open file manager, access to `{your-domain}/file-manager/{guard}/{id}?type=selector`
