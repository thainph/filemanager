<?php
namespace Thainph\Filemanager\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Thainph\Filemanager\Rules\IsValidChunkSize;
use Thainph\Filemanager\Rules\IsValidExtensionByMime;
use Thainph\Filemanager\Rules\IsValidExtensionByName;
use Thainph\Filemanager\Rules\IsValidFileSize;
use Thainph\Filemanager\Rules\IsValidFileSizeByChunkOffset;

class UploadRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'file' => [
                'required',
                'string',
                new IsValidExtensionByMime(),
                new IsValidFileSize(),
            ]
        ];
    }
}
