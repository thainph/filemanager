<?php
namespace Thainph\Filemanager\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Thainph\Filemanager\Rules\IsValidChunkSize;
use Thainph\Filemanager\Rules\IsValidExtensionByName;
use Thainph\Filemanager\Rules\IsValidFileSizeByChunkOffset;

class StoreChunkRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => [
                'required',
                'string',
                new IsValidExtensionByName()
            ],
            'data' => [
                'required',
                'string',
                new IsValidChunkSize(),
            ],
            'offset' => [
                'required',
                'integer',
                new IsValidFileSizeByChunkOffset($this->data, $this->name)
            ],
            'eof' => 'required|bool',
            'hash' => 'nullable|string',
        ];
    }

    public function parameters(): array
    {
        return [
            'data' => $this->input('data'),
            'name' => $this->input('name'),
            'offset' => $this->input('offset'),
            'eof' => $this->input('eof'),
            'hash' => $this->input('hash'),
        ];
    }
}
