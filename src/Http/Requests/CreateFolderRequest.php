<?php
namespace Thainph\Filemanager\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Thainph\Filemanager\Helpers\IdentificationHelper;
use Thainph\Filemanager\Rules\IsBelongToUser;
use Thainph\Filemanager\Rules\IsNotExist;

class CreateFolderRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $guard = $this->route()->parameter('guard');
        $userId = $this->route()->parameter('userId');

        return [
            'folder' => [
                'required',
                'string',
                new IsBelongToUser($guard, $userId),
            ],
            'name' => [
                'required',
                'string',
                'max:255',
                'regex:/^[a-zA-Z0-9-_]+$/',
                new IsNotExist($this->input('folder')),
            ],
        ];
    }
}
