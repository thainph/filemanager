<?php
namespace Thainph\Filemanager\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Thainph\Filemanager\Rules\IsBelongToUser;

class DeleteFilesRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $guard = $this->route()->parameter('guard');
        $userId = $this->route()->parameter('userId');

        return [
            'files' => [
                'required',
                'array'
            ],
            'files.*' => [
                'required',
                'string',
                new IsBelongToUser($guard, $userId),
            ],
        ];
    }
}
