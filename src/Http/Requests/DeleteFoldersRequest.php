<?php
namespace Thainph\Filemanager\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Thainph\Filemanager\Rules\IsBelongToUser;

class DeleteFoldersRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $guard = $this->route()->parameter('guard');
        $userId = $this->route()->parameter('userId');

        return [
            'folders' => [
                'required',
                'array'
            ],
            'folders.*' => [
                'required',
                'string',
                new IsBelongToUser($guard, $userId),
            ],
        ];
    }
}
