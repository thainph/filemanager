<?php

namespace Thainph\Filemanager\Http\Controllers;

use Illuminate\Http\Request;
use Thainph\Filemanager\Helpers\IdentificationHelper;
use Thainph\Filemanager\Helpers\StrHelper;
use Thainph\Filemanager\Http\Requests\CreateFolderRequest;
use Thainph\Filemanager\Http\Requests\DeleteFilesRequest;
use Thainph\Filemanager\Http\Requests\DeleteFoldersRequest;
use Thainph\Filemanager\Http\Requests\StoreChunkRequest;
use Thainph\Filemanager\Http\Requests\UploadRequest;
use Thainph\Filemanager\Services\FileStorageService;

class FileController extends Controller
{
    private FileStorageService $service;

    public function __construct(FileStorageService $service)
    {
        $this->service = $service;
    }

    public function index($guard, $userId, Request $request)
    {
        return view('file-manager::index', [
            'guard' => $guard,
            'userId' => $userId,
            'usingType' => $request->input('type', 'file-manager')
        ]);
    }

    public function storeAnonymousFileByChunk(StoreChunkRequest $request)
    {
        return response()->json($this->service->uploadChunk(
            IdentificationHelper::getAnonymousStorageFolder(),
            $request->input('data'),
            $request->input('name'),
            $request->input('offset'),
            $request->input('eof'),
            $request->input('hash')
        ));
    }

    public function uploadAnonymousFile(UploadRequest $request)
    {
        return response()->json($this->service->uploadFile(
            IdentificationHelper::getAnonymousStorageFolder(),
            $request->file('file')
        ));
    }


    public function getFiles($guard, $userId, Request $request)
    {
        $folder = $request->input('folder', IdentificationHelper::getUserStorageFolder($guard, $userId));

        return response()->json(
            [
                'paths' => StrHelper::explodePathToArray($guard, $userId, $folder),
                'files' => $this->service->getFiles($folder)
            ]
        );
    }

    public function storeFileByChunk($guard, $userId, StoreChunkRequest $request)
    {
        $folder = $request->input('folder', IdentificationHelper::getUserStorageFolder($guard, $userId));
        return response()->json($this->service->uploadChunk(
            $folder,
            $request->input('data'),
            $request->input('name'),
            $request->input('offset'),
            $request->input('eof'),
            $request->input('hash')
        ));
    }

    public function uploadFile($guard, $userId, UploadRequest $request)
    {
        $folder = $request->input('folder', IdentificationHelper::getUserStorageFolder($guard, $userId));
        return response()->json($this->service->uploadFile(
            $folder,
            $request->file('file')
        ));
    }

    public function deleteFiles(DeleteFilesRequest $request)
    {
        $this->service->deleteFiles($request->input('files'));
        return response()->json();
    }


    public function getFolders($guard, $userId, Request $request)
    {
        $folder = $request->input('folder', IdentificationHelper::getUserStorageFolder($guard, $userId));

        return response()->json(
            [
                'paths' => StrHelper::explodePathToArray($guard, $userId, $folder),
                'folders' => $this->service->getFolders($folder)
            ]
        );
    }

    public function createFolder($guard, $userId, CreateFolderRequest $request)
    {
        $folder = $request->input('folder', IdentificationHelper::getUserStorageFolder($guard, $userId));

        return response()->json([
            'name' => $request->name,
            'path' => $this->service->createFolder($folder, $request->name),
            'lastModified' => now()->timestamp
        ]);
    }

    public function deleteFolders(DeleteFoldersRequest $request)
    {
        $this->service->deleteFolders($request->input('folders'));
        return response()->json();
    }
}
