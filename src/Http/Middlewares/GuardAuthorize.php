<?php

namespace Thainph\Filemanager\Http\Middlewares;

use Closure;
use Illuminate\Http\Request;

class GuardAuthorize
{
    public function handle(Request $request, Closure $next)
    {
        $guard = $request->route()->parameter('guard');
        $userId = $request->route()->parameter('userId');

        if (!in_array($guard, config('file-manager.guards'))) {
            abort(404, 'Guard not found');
        }

        $user = auth($guard)->user();

        if (!empty($user->id) && (string)$user->id === (string)$userId) {
            return $next($request);
        }

        abort(403, 'File manager unauthorized');
    }
}
