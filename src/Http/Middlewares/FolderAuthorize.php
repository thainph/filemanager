<?php

namespace Thainph\Filemanager\Http\Middlewares;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Thainph\Filemanager\Helpers\IdentificationHelper;

class FolderAuthorize
{
    public function handle(Request $request, Closure $next)
    {
        $folder = $request->input('folder');

        $guard = $request->route()->parameter('guard');
        $userId = $request->route()->parameter('userId');
        $userFolder = IdentificationHelper::getUserStorageFolder($guard, $userId);

        if (!empty($folder) && !Str::startsWith($folder, $userFolder)) {
            abort(403, 'Unauthorized folder');
        }

        if (empty($folder)) {
            $folder = $userFolder;
        }

        if (!file_exists(IdentificationHelper::getRealPath($userFolder))) {
            mkdir(IdentificationHelper::getRealPath($userFolder), 0777, true);
        }

        if (!file_exists(IdentificationHelper::getRealPath($folder))) {
            abort(404, 'Folder not found');
        }

        return $next($request);
    }
}
