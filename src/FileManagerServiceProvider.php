<?php
namespace Thainph\Filemanager;

use Illuminate\Http\Middleware\HandleCors;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Thainph\Filemanager\Http\Middlewares\GuardAuthorize;
use Thainph\Filemanager\Http\Middlewares\FolderAuthorize;

class FileManagerServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->commands([
            Commands\PublishAssets::class,
        ]);

        $this->publishes([
            __DIR__.'/config/file-manager.php' => config_path('file-manager.php'),
        ], 'file-manager-config');
        $this->publishes([
            __DIR__.'/resources/views' => resource_path('views/vendor/file-manager'),
        ], 'file-manager-views');
        $this->publishes([
            __DIR__.'/resources/lang' => resource_path('lang/vendor/file-manager'),
        ], 'file-manager-lang');
        $this->publishes([
            __DIR__.'/client/assets' => public_path('vendor/file-manager'),
        ], 'file-manager-assets');

        $this->loadRoutesFrom(__DIR__.'/routes/web.php');
        $this->loadViewsFrom(__DIR__.'/resources/views', 'file-manager');
        $this->loadTranslationsFrom(__DIR__.'/resources/lang', 'file-manager');

        $router = $this->app->make(Router::class);
        $router->aliasMiddleware('file_manager.guard_authorize', GuardAuthorize::class);
        $router->aliasMiddleware('file_manager.folder_authorize', FolderAuthorize::class);
        $router->aliasMiddleware('file_manager.cors', HandleCors::class);
    }
}
