<?php
namespace Thainph\Filemanager\Helpers;

use Illuminate\Support\Facades\Storage;

class IdentificationHelper
{
    public static function getUserStorageFolder(string $guard, int $userId): string
    {
        $folder = md5($guard . '_' . $userId);
        return rtrim(config('file-manager.storage.public'), '/') . '/' . $folder;
    }

    public static function getAnonymousStorageFolder(): string
    {
        return rtrim(config('file-manager.storage.public'), '/') . '/anonymous';
    }

    public static function getRealPath(string $path): string
    {
        return storage_path('app/' . ltrim($path, '/'));
    }

    public static function getChunkPartName(string $hash, int $offset): string
    {
        return config('file-manager.storage.private') . '/' . $hash . '.' . $offset . '.part';
    }

    public static function getUniqueFileName(string $fileName): string
    {
        $name = pathinfo($fileName, PATHINFO_FILENAME) . '_' . uniqid();
        $extension = pathinfo($fileName, PATHINFO_EXTENSION);
        return $name . '.' . $extension;
    }

    public static function getPublicUrl(string $path): string
    {
        return rtrim(config('app.url'), '/') . Storage::disk('local')->url($path);
    }

}
