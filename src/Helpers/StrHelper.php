<?php
namespace Thainph\Filemanager\Helpers;

class StrHelper
{
    public static function calculateBase64DataSize(string $base64Data): int
    {
        // Remove the base64 data prefix
        $base64Data = preg_replace('#^data:/\w+;base64,#i', '', $base64Data);

        // Calculate the size of the base64 data
        return (int) ceil(strlen($base64Data) * 3 / 4) - 2;
    }

    public static function explodePathToArray(string $guard, string $user, string $path): array
    {
        $userFolder = IdentificationHelper::getUserStorageFolder($guard, $user);
        $pathParts = explode('/', $path);

        $parentPaths = [];
        $pathAccumulator = '';

        foreach ($pathParts as $part) {
            $pathAccumulator .= $part . '/';
            $parentPath = rtrim($pathAccumulator, '/');

            if ($parentPath !== config('file-manager.storage.public')) {
                $name = explode('/', $parentPath)[count(explode('/', $parentPath)) - 1];
                $parentPaths[] = [
                    'name' => $parentPath === $userFolder ? 'Root' : $name,
                    'path' => $parentPath
                ];
            }
        }

        return $parentPaths;
    }
}
