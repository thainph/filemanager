<?php
namespace Thainph\Filemanager\Helpers;

class AssetHelper
{
    public static function import(): string
    {
        $directory = public_path('vendor/file-manager');
        $allFiles = scandir($directory);
        $jsFiles = array_filter($allFiles, function($file) {
            return preg_match('/\.js$/', $file);
        });
        $cssFiles = array_filter($allFiles, function($file) {
            return preg_match('/\.css$/', $file);
        });

        $scriptTags = '';
        foreach ($jsFiles as $jsFile) {
            $link = asset("/vendor/file-manager/$jsFile");
            $scriptTags .= "<script type='module' defer src='$link'></script>\n";
        }

        $styleTags = '';
        foreach ($cssFiles as $cssFile) {
            $link = asset("/vendor/file-manager/$cssFile");
            $styleTags .= "<link rel='stylesheet' href='$link'>\n";
        }

        return $scriptTags . "\n" . $styleTags;
    }
}
