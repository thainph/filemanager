<?php
return [
    'storage' => [
        'public' => 'public', // Recommended: public
        'private' => 'private' // Private folder name
    ],
    'guards' => [ // Guards list
        'user',
        'admin'
    ],
    'upload' => [
        'chunk_size' => 1024 * 1024 * 2,
        'max_size' => [
            'image' => 1024 * 1024 * 2,
            'video' => 1024 * 1024 * 10,
            'audio' => 1024 * 1024 * 10,
            'document' => 1024 * 1024 * 10
        ],
        'extensions' => [
            'image' => ['jpg', 'jpeg', 'png', 'gif', 'svg', 'webp'],
            'video' => ['mp4', 'avi', 'mov', 'wmv', 'flv', '3gp', 'webm'],
            'audio' => ['mp3', 'wav', 'ogg', 'flac', 'aac', 'wma'],
            'document' => ['pdf', 'doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx', 'txt', 'csv']
        ]
    ],
];
