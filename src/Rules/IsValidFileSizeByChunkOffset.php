<?php

namespace Thainph\Filemanager\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use Thainph\Filemanager\Helpers\StrHelper;

class IsValidFileSizeByChunkOffset implements ValidationRule
{
    protected string $base64Data;
    protected string $fileName;

    public function __construct(string $base64Data, string $fileName)
    {
        $this->base64Data = $base64Data;
        $this->fileName = $fileName;
    }
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $chunkSize = StrHelper::calculateBase64DataSize($this->base64Data);
        $totalSize = ((int)$value + 1) * config('file-manager.upload.chunk_size') - config('file-manager.upload.chunk_size') + $chunkSize;
        $extension = strtolower(pathinfo($this->fileName, PATHINFO_EXTENSION));
        $maxSize = 1024 * 1024 * 2;

        foreach (config('file-manager.upload.extensions') as $key => $extensions) {
            if (in_array($extension, $extensions)) {
                $maxSize = config('file-manager.upload.max_size')[$key];
                break;
            }
        }

        if ($totalSize > $maxSize) {
            $fail(trans('file-manager::validation.file_size_is_too_large', [
                'max_size' => $maxSize / 1024 / 1024,
            ]));
        }
    }
}
