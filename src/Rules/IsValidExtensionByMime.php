<?php

namespace Thainph\Filemanager\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class IsValidExtensionByMime implements ValidationRule
{
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $mimeType = $value->getMimeType();
        $extension = explode('/', $mimeType)[1];
        $isPass = false;

        foreach (config('file-manager.upload.extensions') as $extensions) {
            if (in_array($extension, $extensions)) {
                $isPass = true;
                break;
            }
        }

        if (!$isPass) {
            $fail(trans('file-manager::validation.file_extension_is_not_allowed', [
                'attribute' => $attribute,
            ]));
        }
    }
}
