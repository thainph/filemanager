<?php

namespace Thainph\Filemanager\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Support\Str;
use Thainph\Filemanager\Helpers\IdentificationHelper;

class IsBelongToUser implements ValidationRule
{
    protected $guard;
    protected $userId;

    public function __construct($guard, $userId)
    {
        $this->guard = $guard;
        $this->userId = $userId;
    }

    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $userFolder = IdentificationHelper::getUserStorageFolder($this->guard, $this->userId);

        if (!Str::startsWith($value, $userFolder)) {
            $fail(trans('file-manager::validation.file_is_not_belong_to_user'));
        }
    }
}
