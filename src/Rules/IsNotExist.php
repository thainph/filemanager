<?php

namespace Thainph\Filemanager\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Support\Str;
use Thainph\Filemanager\Helpers\IdentificationHelper;

class IsNotExist implements ValidationRule
{
    protected string $folder;

    public function __construct($folder)
    {
        $this->folder = $folder;
    }

    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $path = $this->folder. '/' . $value;
        $realPath = IdentificationHelper::getRealPath($path);

        if (file_exists($realPath)) {
            $fail(trans('file-manager::validation.folder_is_exist'));
        }
    }
}
