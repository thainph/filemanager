<?php

namespace Thainph\Filemanager\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class IsValidExtensionByName implements ValidationRule
{
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        // Get the file extension
        $extension = strtolower(pathinfo($value, PATHINFO_EXTENSION));
        $isPass = false;

        foreach (config('file-manager.upload.extensions') as $extensions) {
            if (in_array($extension, $extensions)) {
                $isPass = true;
                break;
            }
        }

        if (!$isPass) {
            $fail(trans('file-manager::validation.file_extension_is_not_allowed', [
                'attribute' => $attribute,
            ]));
        }
    }
}
