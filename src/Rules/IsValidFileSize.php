<?php

namespace Thainph\Filemanager\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class IsValidFileSize implements ValidationRule
{

    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $mimeType = $value->getMimeType();
        $extension = explode('/', $mimeType)[1];
        $maxSize = 1024 * 1024 * 2;
        $fileSize = $value->getSize();

        foreach (config('file-manager.upload.extensions') as $key => $extensions) {
            if (in_array($extension, $extensions)) {
                $maxSize = config('file-manager.upload.max_size')[$key];
                break;
            }
        }

        if ($fileSize > $maxSize) {
            $fail(trans('file-manager::validation.file_size_is_too_large', [
                'max_size' => $maxSize / 1024 / 1024,
            ]));
        }
    }
}
