<?php

namespace Thainph\Filemanager\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use Thainph\Filemanager\Helpers\StrHelper;

class IsValidChunkSize implements ValidationRule
{
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $size = StrHelper::calculateBase64DataSize($value);

        if ($size > config('file-manager.upload.chunk_size')) {
            $fail(trans('file-manager::validation.file_size_is_too_large', [
                'max_size' => config('file-manager.upload.chunk_size'),
            ]));
        }
    }
}
