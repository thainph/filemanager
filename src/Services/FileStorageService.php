<?php
namespace Thainph\Filemanager\Services;

use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Thainph\Filemanager\Helpers\IdentificationHelper;

class FileStorageService
{
    private Filesystem $storage;

    public function __construct()
    {
        $this->storage = Storage::disk('local');
    }

    public function getFiles(string $folder): array
    {
        $files = $this->storage->files($folder);

        return array_map(function ($file) {
            return [
                'name' => explode('/', $file)[count(explode('/', $file)) - 1],
                'path' => $file,
                'url' => IdentificationHelper::getPublicUrl($file),
                'size' => $this->storage->size($file),
                'lastModified' => $this->storage->lastModified($file),
                'type' => $this->storage->mimeType($file)
            ];
        },$files);

    }

    public function uploadFile(string $folder, UploadedFile $file): array
    {
        $fileName = IdentificationHelper::getUniqueFileName($file->getClientOriginalName());
        $filePath = rtrim($folder, '/') . '/' . $fileName;
        $this->storage->put($filePath, file_get_contents($file->getRealPath()));

        return [
            'url' => IdentificationHelper::getPublicUrl($filePath),
            'name' => $fileName
        ];
    }

    public function uploadChunk(
        string $folder,
        string $base64Data,
        string $name,
        int $offset,
        bool $eof,
        string $hash = null
    ): array
    {
        $hash = $hash ?? uniqid() . Str::random(15);
        $chunkPartName = IdentificationHelper::getChunkPartName($hash, $offset);
        $this->storage->put($chunkPartName, $base64Data);

        if ($eof) {
            $file = $this->buildFileFromChunks($folder, $name, $hash, $offset);

            return [
                'name' => explode('/', $file)[count(explode('/', $file)) - 1],
                'path' => $file,
                'url' => IdentificationHelper::getPublicUrl($file),
                'size' => $this->storage->size($file),
                'lastModified' => $this->storage->lastModified($file),
                'type' => $this->storage->mimeType($file)
            ];
        }

        return [
            'url' => null,
            'name' => $name,
            'hash' => $hash,
            'offset' => $offset,
            'eof' => false
        ];
    }

    private function buildFileFromChunks(string $folder, string $fileName, string $hash, int $maxOffset): string
    {
        $fileName = IdentificationHelper::getUniqueFileName($fileName);
        $fileName = rtrim($folder, '/') . '/' . $fileName;
        $this->storage->put($fileName, '');
        $file = fopen(IdentificationHelper::getRealPath($fileName), 'a+');

        // Chunk start from 0
        for ($i = 0; $i <= $maxOffset; $i++) {
            $chunkName = IdentificationHelper::getChunkPartName($hash, $i);
            $chunkData = base64_decode($this->storage->get($chunkName));
            fwrite($file, $chunkData);
            $this->storage->delete($chunkName);
        }

        fclose($file);

        return $fileName;
    }

    public function deleteFiles(array $files): void
    {
        foreach ($files as $file) {
            $this->storage->delete($file);
        }
    }

    public function getFolders(string $folder): array
    {
        $directories = $this->storage->directories($folder);

        return array_map(function ($directory) {
            return [
                'name' => explode('/', $directory)[count(explode('/', $directory)) - 1],
                'path' => $directory,
                'lastModified' => $this->storage->lastModified($directory),
            ];
        },$directories);
    }

    public function createFolder(string $folder, string $name): string
    {
        $folder = rtrim($folder, '/') . '/' . $name;
        $this->storage->makeDirectory($folder);

        return $folder;
    }

    public function deleteFolders(array $folders): void
    {
        foreach ($folders as $folder) {
            $this->storage->deleteDirectory($folder);
        }
    }

}
