<?php
namespace Thainph\Filemanager\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class PublishAssets extends Command
{
    protected $signature = 'file-manager:publish-assets';
    protected $description = 'Publish the file manager assets and remove old files';

    public function handle()
    {
        $targetPath = public_path('vendor/file-manager');

        // Check if the directory exists and remove it
        if (File::exists($targetPath)) {
            File::deleteDirectory($targetPath);
        }

        // Run the vendor publish command
        $this->call('vendor:publish', [
            '--tag' => 'file-manager-assets',
            '--force' => true // to overwrite any existing files
        ]);
    }
}
