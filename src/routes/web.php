<?php

use Illuminate\Support\Facades\Route;


Route::group([
    'middleware' => [
        'web',
        'file_manager.cors'
    ],
    'prefix' => 'file-manager/anonymous',
    'as' => 'file_manager.',
    'namespace' => 'Thainph\Filemanager\Http\Controllers'
], function() {
    Route::post('/files', 'FileController@storeAnonymousFileByChunk')->name('storeAnonymousFile');
    Route::post('/upload', 'FileController@uploadAnonymousFile')->name('uploadAnonymousFile');
});


Route::group([
    'middleware' => [
        'web',
        'file_manager.guard_authorize',
        'file_manager.folder_authorize',
        'file_manager.cors'
    ],
    'prefix' => 'file-manager/{guard}/{userId}',
    'as' => 'file_manager.',
    'namespace' => 'Thainph\Filemanager\Http\Controllers'
], function() {
    Route::get('/', 'FileController@index')->name('index');
    Route::get('/files', 'FileController@getFiles')->name('getFiles');
    Route::post('/files', 'FileController@storeFileByChunk')->name('storeFile');
    Route::delete('/files', 'FileController@deleteFiles')->name('deleteFiles');
    Route::get('/folders', 'FileController@getFolders')->name('getFolders');
    Route::post('/folders', 'FileController@createFolder')->name('createFolder');
    Route::delete('/folders', 'FileController@deleteFolders')->name('deleteFolders');
    Route::post('/upload', 'FileController@uploadFile')->name('uploadFile');
});
