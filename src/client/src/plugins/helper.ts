
export default {
    bindingAppState(key: string, parse: boolean = false) {
        //@ts-ignore
        if (window.FileManager && window.FileManager[key]) {
            //@ts-ignore
            return parse ? JSON.parse(window.FileManager[key]) : window.FileManager[key];
        }

        return null;
    },
    getRoute(endpoint: string) {
        const domain = this.bindingAppState('domain');
        const guard = this.bindingAppState('guard');
        const userId = this.bindingAppState('userId');
        const baseUrl = `${domain}/file-manager/${guard}/${userId}`;

        return `${baseUrl}/${endpoint}`;
    },
    getLocale() {
        const html = document.querySelector('html');
        return html ? html.getAttribute('lang') : 'en';
    },
    getCsrfToken() {
        const csrfToken = document.querySelector('meta[name="csrf-token"]')
        return csrfToken ? csrfToken.getAttribute('content') : ''
    },
}
