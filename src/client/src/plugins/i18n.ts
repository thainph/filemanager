import en from '../lang/en.json';
import ja from '../lang/ja.json';
import vi from '../lang/vi.json';
import Helper from "./helper";
import { createI18n } from "vue-i18n";

const messages = { en, ja, vi };
const i18n = createI18n({
    legacy:  false,
    locale: Helper.getLocale() as string,
    messages: messages,
    fallbackLocale: 'en'
});
const t = i18n.global.t;
export { t };
export default i18n;
