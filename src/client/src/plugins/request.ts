import axios from 'axios';
import _ from 'lodash';
import Helper from './helper';

export type RequestHeaderConfig = {
    withLocale?: boolean,
    withCsrf?: boolean,
    withCors?: boolean,
    withFormUpload?: boolean,
}

const buildHeaders = (headers: any = {}, config: RequestHeaderConfig = {}) => {
    if (config.withLocale) {
        headers = {
            ...headers,
            'X-LOCALE': Helper.getLocale(),
        };
    }

    if (config.withCsrf) {
        headers = {
            ...headers,
            'X-CSRF-TOKEN': Helper.getCsrfToken(),
        };
    }

    if (config.withCors) {
        headers = {
            ...headers,
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Headers': '*',
            'Access-Control-Allow-Methods': '*',
            'Sec-Fetch-Site': 'cross-site',
            'Sec-Fetch-Mode': 'no-cors',
        };
    }

    if (config.withFormUpload) {
        headers = {
            ...headers,
            'Content-Type': 'multipart/form-data',
        };
    }

    return headers
}

export default {
    get: (uri: string, data: any = {}) => {
        let headers = data.headers || {};
        let config: RequestHeaderConfig = data.config || {
            withLocale: true,
            withCsrf: true,
        };
        headers = buildHeaders(headers, config);
        return axios.get(uri, {headers});
    },

    upload: (uri: string, payload: any = {}, data: any = {}) => {
        let headers = data.headers || {};
        let config: RequestHeaderConfig = data.config || {
            withLocale: true,
            withCsrf: true,
            withFormUpload: true
        };
        headers = buildHeaders(headers, config);

        return axios.post(uri, payload, {headers})
    },

    post: (uri: string, payload: any = {}, data: any = {}) => {
        let headers = data.headers || {};
        let config: RequestHeaderConfig = data.config || {
            withLocale: true,
            withCsrf: true
        };
        headers = buildHeaders(headers, config);

        return axios.post(uri, payload, {headers})
    },

    put: (uri: string, payload: any = {}, data: any = {}) => {
        let headers = data.headers || {};
        let config: RequestHeaderConfig = data.config || {
            withLocale: true,
            withCsrf: true
        };
        headers = buildHeaders(headers, config);

        return axios.put(uri, payload, {headers})
    },

    patch: (uri: string, payload: any = {}, data: any = {}) => {
        let headers = data.headers || {};
        let config: RequestHeaderConfig = data.config || {
            withLocale: true,
            withCsrf: true
        };
        headers = buildHeaders(headers, config);

        return axios.patch(uri, payload, {headers})
    },

    delete: (uri: string, data: any = {}) => {
        let headers = data.headers || {};
        let config: RequestHeaderConfig = data.config || {
            withLocale: true,
            withCsrf: true
        };
        headers = buildHeaders(headers, config);

        return axios.delete(uri, {headers})
    },

    download: async (uri: string, method: string, data: any, file: string) => {
        const response = await axios.request({
            url: uri,
            method: method,
            responseType: 'blob',
            data,
        });
        const downloadUrl = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement('a');
        link.href = downloadUrl;
        link.setAttribute('download', file);
        document.body.appendChild(link);
        link.click();
        link.remove();
    },

    errors: (errorResponse: any) => {
        const errorsData = _.get(errorResponse, 'data.errors')

        if (!_.isEmpty(errorsData)) {
            return Object.values(errorsData)
        }

        const error = _.get(errorResponse, 'data.error');

        if (error) {
            return [error]
        }

        const message = _.get(errorResponse, 'data.message')

        if (message) {
            return [message];
        }

        return ['Something went wrong. Try again later!'];
    },
};
