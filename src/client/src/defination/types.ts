import {NotificationStore} from "modable";

export type Notification = typeof NotificationStore;
export type FileData = {
    name: string,
    path: string,
    type: string,
    size: number,
    url: string,
    lastModified: number,
}
export type UserFile = {
    name: string,
    path: string,
    type: string,
    size: number,
    url: string,
    lastModified: number,
    isImage: boolean,
    isVideo: boolean,
    isDocument: boolean,
    isHidden: boolean,
    isSelected: boolean,
}
export type UserFolder = {
    name: string,
    path: string,
    lastModified: string,
}
export type UserPath = {
    name: string,
    path: string,
}
