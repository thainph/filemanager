import {reactive, readonly } from 'vue';

const state = reactive({
    loading: false
})

const actions = {
    showLoader() {
        state.loading = true
    },
    hideLoader() {
        state.loading = false
    }
}

const getters = {
    isLoading() {
        return state.loading;
    }
}
export default {
    state: readonly(state),
    actions,
    getters
}
