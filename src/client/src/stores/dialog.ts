import {reactive, readonly } from 'vue';

export type ConfirmDialogConfig = {
    message: string,
    confirmLabel: string,
    cancelLabel: string,
    onConfirm: Function,
}
const state = reactive({
    visible: false,
    message: 'Are you sure?',
    confirmLabel: 'Yes',
    cancelLabel: 'No',
    onConfirm: () => {},
})

const actions = {
    show(config: ConfirmDialogConfig) {
        state.visible = true
        state.message = config.message
        state.confirmLabel = config.confirmLabel
        state.cancelLabel = config.cancelLabel
        // @ts-ignore
        state.onConfirm = config.onConfirm
    },
    hide() {
        state.visible = false
    },
    async confirm() {
        await state.onConfirm()
        actions.hide()
    }

}

const getters = {
    visible() {
        return state.visible;
    },
    message() {
        return state.message;
    },
    confirmLabel() {
        return state.confirmLabel;
    },
    cancelLabel() {
        return state.cancelLabel;
    }
}
export default {
    state: readonly(state),
    actions,
    getters
}
