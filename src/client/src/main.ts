import { createApp } from 'vue'
import App from './App.vue'
import Modable from 'modable'
import './style.css'
import LoaderState from './stores/loader';
import DialogState from './stores/dialog';
import VueI18n from "./plugins/i18n"

const app = createApp(App)
app.provide('LoaderState', LoaderState);
app.provide('DialogState', DialogState);
app.use(Modable)
app.use(VueI18n);
app.mount('#file-manager-app')
