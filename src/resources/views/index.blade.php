<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ trans('file-manager::main.title') }}</title>
    {!!  \Thainph\Filemanager\Helpers\AssetHelper::import() !!}
</head>
<body>
<div id="file-manager-app"></div>
<script>
    window.FileManager = {
        domain: `{{ config('app.url') }}`,
        guard: `{{ $guard }}`,
        userId: `{{ $userId }}`,
        chunkSize: {{ config('file-manager.upload.chunk_size') }},
        mimeTypes: `{{ \Thainph\Filemanager\Helpers\MimeTypeHelper::allowedMimeTypes() }}`,
        usingType: `{{ $usingType }}`,
    }
</script>
</body>
</html>
