<?php
return [
    'file_size_is_too_large' => 'The file size is too large. The maximum file size is :max_size mb',
    'file_extension_is_not_allowed' => 'The file extension is not allowed.',
    'file_is_not_belong_to_user' => 'The file is not belong to user.',
    'folder_is_exist' => 'The folder is exist.',
];
